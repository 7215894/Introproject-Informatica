using SolverLogic;

namespace UI;

public class VSSudoku {
    public byte[] sudoku = new byte[81];

    public (int, int) selField = (0, 0);
    public bool selValid = false;
    
    private readonly Stack<(int, int, byte)> undoStack = new Stack<(int, int, byte)>();
    private readonly Stack<(int, int, byte)> redoStack = new Stack<(int, int, byte)>();

    private readonly VsSolver vsSolver = new();
    private readonly McvSolver solver = new();

    
    // Use this indexer for operations that should be undoable
    public byte? this[int x, int y] {
        get {
            var value = sudoku[9 * y + x];
            return value == 0 ? null : value;
        }
        set {
            if(!ValidateMove(9 * y + x, value)) return;
            undoStack.Push((x, y, sudoku[9 * y + x]));
            sudoku[9 * y + x] = value ?? 0;
            redoStack.Clear();
        }
    }

    public byte? selectedNumber {
        get {
            if (!selValid) return 0;
            (int x, int y) = selField;
            return this[x, y];
        }
        set {
            if (!selValid) return;
            (int x, int y) = selField;
            this[x, y] = value;
        }
    }

    public void Undo() {
        if (undoStack.Count == 0) return;
        var (x, y, oldVal) = undoStack.Pop();
        var newVal = sudoku[9 * y + x];
        sudoku[9 * y + x] = oldVal;
        redoStack.Push((x, y, newVal));
    }

    public void Redo() {
        if (redoStack.Count == 0) return;
        var (x, y, newVal) = redoStack.Pop();
        var oldVal = sudoku[9 * y + x];
        sudoku[9 * y + x] = newVal;
        undoStack.Push((x, y, oldVal));
    }
    
    public bool Solve() {
        byte[]? solution = solver.GetSolution(sudoku);
        if (solution == null) return false;
        sudoku = solution;
        return true;
    }

    // This operation cannot be undone
    public void ClearAll() {
        Array.Clear(sudoku, 0, 81);
        undoStack.Clear();
        redoStack.Clear();
        selValid = false;
    }

    public bool CheckWinner() {
        byte[] first = new byte[81];
        byte[] second = new byte[81];
        Array.Copy(sudoku, first, 81);
        Array.Copy(sudoku, second, 81);
        bool solvable = !vsSolver.IsNotSolvable(first);
        bool unique = vsSolver.IsUniquelySolvable(second);
        return unique && solvable;
    }

    public bool ValidateMove(int index, byte? move) {
        if (move is null) return true;
        byte mmove = move ?? 1;
        byte[] after = new byte[81];
        Array.Copy(sudoku, after, 81);
        return SudokuUtils.IsValidMove(mmove, index, after) &&
               solver.GetSolution(after) is not null;
    }
}
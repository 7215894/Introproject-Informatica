namespace UI;

public class Field: Button {
    public int x;
    public int y;

    public byte digit {
        set => Text = value != 0 ? value.ToString() : "";
    }

    public Field() {
        BackgroundColor = Colors.White;
        TextColor = Colors.Black;
        FontSize = 35;
    }
}

﻿using Microsoft.Maui.Controls.Compatibility.Platform;
using SolverLogic;
using System.Drawing;
using System.Runtime.CompilerServices;

namespace UI;

public partial class MainPage {
    Button? selectedField;
    double lengte = 300;

	public MainPage()
	{
		InitializeComponent();
	}

    private async void ToSudokuSolver(object? sender, EventArgs eventArgs)
    {
	    try {
		    await Shell.Current.GoToAsync(nameof(SudokuSolver));
	    }
	    catch (Exception e) {
		    await DisplayAlert("crash", e.ToString(), "F");
	    }
    }

    private async void ToSelectOpponent(object? sender, EventArgs eventArgs)
    {
        await Shell.Current.GoToAsync(nameof(SelectOpponent));
    }

	private void MaximizeWindow(object? sender, EventArgs eventArgs)
	{
		Window.X = 0;
		Window.Y = 0;
		double Width = DeviceDisplay.Current.MainDisplayInfo.Width;
        double Height = DeviceDisplay.Current.MainDisplayInfo.Height;
		Window.Height = Height;
		Window.Width = Width;
    }
}
﻿namespace UI;

public partial class App {
	public App()
	{
		InitializeComponent();

		MainPage = new AppShell();
	}
}

namespace UI;

public partial class SelectOpponent : ContentPage
{
	public SelectOpponent()
	{
		InitializeComponent();
	}

    private async void PlayAgainstPerson (object? sender, EventArgs eventArgs)
    {
        await Shell.Current.GoToAsync(nameof(VersusSudoku));
    }

    private async void PlayAgainstBot(object? sender, EventArgs eventArgs)
    {
        await Shell.Current.GoToAsync(nameof(VersusBot));
    }
}
namespace UI;

public class Input : Button {
    // Keeps track of which number it represents
    private byte _digit = 0;
    public byte digit {
        get => _digit;
        set {
            _digit = value;
            Text = value.ToString();
        }
    }

    public Input() {
        BackgroundColor = Colors.Aqua;
        TextColor = Colors.Black;
        FontSize = 20;
    }
}
﻿namespace UI;

public partial class AppShell {
	public AppShell()
	{
		InitializeComponent();

		Routing.RegisterRoute(nameof(SudokuSolver), typeof(SudokuSolver));
		Routing.RegisterRoute(nameof(SelectOpponent), typeof(SelectOpponent));
		Routing.RegisterRoute(nameof(VersusSudoku), typeof(VersusSudoku));
		Routing.RegisterRoute(nameof(VersusBot), typeof(VersusBot));
		
	}
}

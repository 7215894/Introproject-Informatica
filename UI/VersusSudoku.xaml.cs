using SolverLogic;

namespace UI;

public partial class VersusSudoku : ContentPage {
    private double lengte = 300;

    private VSSudoku sudokuClass = new();
    private readonly McvSolver solver = new McvSolver();

    private string namePlayer1 = "Player 1";
    private string namePlayer2 = "Player 2";
    private int playerToMove = 1;

    private Color standard = Colors.White;
    private Color highlight = Colors.Aqua;
    private Color superhighlight = Colors.HotPink;
    private Color numberButton = Color.FromRgb(0, 255, 255);
    private Color otherButton = Color.FromRgb(255, 0, 255);

    private Field? selectedField {
        get => sudokuClass.selValid
            ? GetField(sudokuClass.selField.Item1, sudokuClass.selField.Item2)
            : null;
        set {
            ClearHighlights();
            if (value == null) {
                sudokuClass.selValid = false;
                return;
            }

            sudokuClass.selValid = true;

            sudokuClass.selField.Item1 = value.x;
            sudokuClass.selField.Item2 = value.y;
            HighlightRowColumn(value.x, value.y);
            HighlightBlock(value);
            value.BackgroundColor = superhighlight;
        }
    }

    public VersusSudoku() {
        InitializeComponent();
        for (byte i = 1; i <= 9; i++) {
            var but = (Input)inputGrid[i - 1];
            but.digit = i;
            but.Clicked += NumberButtonClicked;
        }

        // Enters coordinates for the fields
        for (var i = 0; i < 9; i++) {
            for (var j = 0; j < 9; j++) {
                var field = GetField(i, j);
                field.x = i;
                field.y = j;
            }
        }
    }

    private static (int, int) ConvertCoordinate(int x, int y) {
        return (3 * (y / 3) + x / 3, 3 * (y % 3) + x % 3);
    }

    private Field GetField(int x, int y) {
        var (big, small) = ConvertCoordinate(x, y);
        return (Field)((Grid)SudokuGrid[big])[small];
    }

    private void ResizeSudokuGrid(object sender, EventArgs e) {
        var x = Window.Width;
        var y = Window.Height;
        double l;
        if (x >= y)
            l = y / 10 * 7;
        else
            l = x / 10 * 7;
        SudokuGrid.WidthRequest = l;
        SudokuGrid.HeightRequest = l;
    }

    private void Selected(object sender, EventArgs e) {
        selectedField = (Field)sender;
    }

    private void NumberButtonClicked(object sender, EventArgs e) {
        if (selectedField != null) selectedField.Text = ((Button)sender).Text;
        EndTurn();
    }

    private void MenuNumberClicked(object sender, EventArgs e) {
        if (selectedField != null)
            selectedField.Text = ((MenuFlyoutItem)sender).Text;
        EndTurn();
    }

    private void Delete(object sender, EventArgs e) {
        if (selectedField != null) selectedField.Text = string.Empty;
    }

    private void Reload() {
        for (int i = 0; i < 9; i++) {
            for (int j = 0; j < 9; j++) {
                GetField(i, j).digit = sudokuClass[i, j] ?? 0;
            }
        }
    }

    private void ClearAll(object? sender, EventArgs eventArgs) {
        selectedField = null;
        foreach (Grid subgrid in SudokuGrid)
        foreach (Button field in subgrid)
            field.Text = "";
        sudokuClass = new VSSudoku();
    }

    private async void NamePopup(object sender, EventArgs e) {
        string answerPlayer1 = await DisplayPromptAsync("",
            "What is the name of player 1?", initialValue: "Player 1");
        string answerPlayer2 = await DisplayPromptAsync("",
            "What is the name of player 2?", initialValue: "Player 2");
        namePlayer1 = answerPlayer1;
        namePlayer2 = answerPlayer2;
        playerLabel.Text = $"{namePlayer1} to move";
    }


    private void EndTurn() {
        CheckWinner();
        ChangePlayer();
    }

    private async void CheckWinner()
    {
        bool unique = sudokuClass.CheckWinner();
        if (unique)
        {
            uniqueLabel.Text = "It is Uniquely solvable!";
            if (playerToMove == 1)
            {
                await DisplayAlert($"Game over", $"{namePlayer1} has won!", "Ok");
            }
            else
            {
                await DisplayAlert($"Game over", $"{namePlayer2} has won!", "Ok");
            }
        }
    }

    private void ChangePlayer() {
        switch (playerToMove) {
        case 1:
            playerLabel.Text = $"{namePlayer2} to move";
            playerToMove = 2;
            break;
        case 2:
            playerLabel.Text = $"{namePlayer1} to move";
            playerToMove = 1;
            break;
        }
    }

    void Undo(object? sender, EventArgs e) {
        sudokuClass.Undo();
        Reload();
    }

    void Redo(object? sender, EventArgs e) {
        sudokuClass.Redo();
        Reload();
    }

    // Methods for highlighting
    private void ClearHighlights() {
        foreach (Grid subgrid in SudokuGrid) {
            foreach (Field field in subgrid) {
                field.BackgroundColor = standard;
            }
        }
    }

    private void HighlightBlock(Field field) {
        foreach (Field neighbor in ((Grid)field.Parent).Children) {
            neighbor.BackgroundColor = highlight;
        }
    }

    private void HighlightRowColumn(int x, int y) {
        for (int i = 0; i < 9; i++) {
            GetField(x, i).BackgroundColor = highlight;
        }

        for (int i = 0; i < 9; i++) {
            GetField(i, y).BackgroundColor = highlight;
        }
    }
}
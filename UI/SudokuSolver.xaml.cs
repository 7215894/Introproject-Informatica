using System.Diagnostics;
using SolverLogic;
using SolverLogic.bin;
using System.Diagnostics;

namespace UI;

public partial class SudokuSolver {
    private double lengte = 300;

    private readonly Sudoku sudoku = new();
    private VsBot bot = new RandomBot();

    private Color standard = Colors.White;
    private Color highlight = Colors.Aqua;
    private Color superhighlight = Colors.HotPink;
    private Color numberButton = Color.FromRgb(0, 255, 255);
    private Color otherButton = Color.FromRgb(255, 0, 255);

    private Field? selectedField {
        get => sudoku.selValid
            ? GetField(sudoku.selField.Item1, sudoku.selField.Item2)
            : null;
        set {
            ClearHighlights();
            if (value == null) {
                sudoku.selValid = false;
                return;
            }

            sudoku.selValid = true;

            sudoku.selField.Item1 = value.x;
            sudoku.selField.Item2 = value.y;
            HighlightRowColumn(value.x, value.y);
            HighlightBlock(value);
            value.BackgroundColor = superhighlight;
        }
    }

    public SudokuSolver() {
        InitializeComponent();
        // Initializes values of the buttons under the sudoku
        for (byte i = 1; i <= 9; i++) {
            var but = (Input)inputGrid[i - 1];
            but.digit = i;
            but.Clicked += NumberButtonClicked!;
        }

        // Enters coordinates for the fields
        for (var i = 0; i < 9; i++) {
            for (var j = 0; j < 9; j++) {
                var field = GetField(i, j);
                field.x = i;
                field.y = j;
            }
        }
    }

    private static (int, int) ConvertCoordinate(int x, int y) {
        return (3 * (y / 3) + x / 3, 3 * (y % 3) + x % 3);
    }

    private Field GetField(int x, int y) {
        var (big, small) = ConvertCoordinate(x, y);
        return (Field)((Grid)SudokuGrid[big])[small];
    }

    private void ResizeSudokuGrid(object sender, EventArgs e) {
        var x = Window.Width;
        var y = Window.Height;
        double l;
        if (x >= y)
        {
            l = y / 4 * 3;
        }
        else
        {
            l = x / 4 * 3;
        }
        SudokuGrid.WidthRequest = l;
        SudokuGrid.HeightRequest = l;
    }

    private void Selected(object sender, EventArgs e) {
        selectedField = (Field)sender;
    }

    private void NumberButtonClicked(object sender, EventArgs e) {
        sudoku.selectedNumber = ((Input)sender).digit;
        Reload();
    }

    private void MenuNumberClicked(object sender, EventArgs e) {
        sudoku.selectedNumber = byte.Parse(((MenuFlyoutItem)sender).Text);
        Reload();
    }

    
    private void Delete(object sender, EventArgs e) {
        if (selectedField == null) return;
        selectedField.digit = 0;
        sudoku.selectedNumber = 0;
    }

    private async void Solve(object? sender, EventArgs e) {
        if (!sudoku.Solve()) {
            await DisplayAlert("Unsolvable",
                "There are no solutions to this sudoku", "OK");
            return;
        }

        Reload();
    }

    private void Reload() {
        for (int i = 0; i < 9; i++) {
            for (int j = 0; j < 9; j++) {
                GetField(i, j).digit = sudoku[i, j] ?? 0;
            }
        }
    }

    private void ClearAll(object? sender, EventArgs eventArgs) {
        sudoku.ClearAll();
        Reload();
        ClearHighlights();
    }

    void Undo(object? sender, EventArgs e) {
        sudoku.Undo();
        Reload();
    }

    void Redo(object? sender, EventArgs e) {
        sudoku.Redo();
        Reload();
    }
    
    // Method for selecting what type of hint they want
    private async void SelectHint(object? sender, EventArgs e)
    {
        string hintType = await DisplayActionSheet("What type of hint?", "cancel", null, "Random Hint", "Small Hint", "Big Hint");
        if (hintType == null) { return; }
        Hinter hint = new Hinter();
        if (hintType == "Random Hint")
        {
            sudoku.sudoku = hint.GetRandomHint(sudoku.sudoku);
            Debug.WriteLine("Random Hint");
            Reload();
        }
        else if (hintType == "Small Hint")
        {
            sudoku.sudoku = hint.GetSmallHint(sudoku.sudoku);
            Debug.WriteLine("Small Hint");
            Reload();
        }
        else if (hintType == "Big Hint")
        {
            sudoku.sudoku = hint.GetBigHint(sudoku.sudoku);
            Debug.WriteLine("Big Hint");
            Reload();
        }
    }

    private async void SelectDifficulty(object? sender, EventArgs e)
    {
        string difficulty = await DisplayActionSheet("What sudoku difficulty?", "cancel", null, "easy", "medium", "hard");
        if (difficulty == null) { return; }
        SudokuGenerator generator = new SudokuGenerator();
        if (difficulty == "easy")
        {
            sudoku.sudoku = generator.GenerateSudoku(38);
            Reload();
        }
        else if (difficulty == "medium")
        {
            sudoku.sudoku = generator.GenerateSudoku(32);
            Reload();
        }
        else if (difficulty == "hard")
        {
            sudoku.sudoku = generator.GenerateSudoku(27);
            Reload();
        }
    }

    // Methods for highlighting
    private void ClearHighlights() {
        foreach (Grid subgrid in SudokuGrid) {
            foreach (Field field in subgrid) {
                field.BackgroundColor = standard;
            }
        }
    }

    private void HighlightBlock(Field field) {
        foreach (Field neighbor in ((Grid)field.Parent).Children) {
            neighbor.BackgroundColor = highlight;
        }
    }

    private void HighlightRowColumn(int x, int y) {
        for (int i = 0; i < 9; i++) {
            GetField(x, i).BackgroundColor = highlight;
        }

        for (int i = 0; i < 9; i++) {
            GetField(i, y).BackgroundColor = highlight;
        }
    }

}
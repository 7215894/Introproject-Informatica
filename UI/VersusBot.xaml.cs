using Microsoft.Extensions.Configuration;
using SolverLogic;
using System.Diagnostics;

namespace UI;

public partial class VersusBot : ContentPage
{
    private double lengte = 300;

    private VSSudoku sudokuClass = new();

    private VsBot bot = new RandomBot();

    private string namePlayer1 = "Player 1";
    private string namePlayer2 = "Bot";
    private int playerToMove = 1;

    private Color standard = Colors.White;
    private Color highlight = Colors.Aqua;
    private Color superhighlight = Colors.HotPink;
    private Color numberButton = Color.FromRgb(0, 255, 255);
    private Color otherButton = Color.FromRgb(255, 0, 255);

    private Field? selectedField
    {
        get => sudokuClass.selValid
            ? GetField(sudokuClass.selField.Item1, sudokuClass.selField.Item2)
            : null;
        set
        {
            ClearHighlights();
            if (value == null)
            {
                sudokuClass.selValid = false;
                return;
            }

            sudokuClass.selValid = true;

            sudokuClass.selField.Item1 = value.x;
            sudokuClass.selField.Item2 = value.y;
            HighlightRowColumn(value.x, value.y);
            HighlightBlock(value);
            value.BackgroundColor = superhighlight;
        }
    }

    public VersusBot()
    {
        InitializeComponent();
        for (byte i = 1; i <= 9; i++)
        {
            var but = (Input)inputGrid[i - 1];
            but.digit = i;
            but.Clicked += NumberButtonClicked;
        }

        // Enters coordinates for the fields
        for (var i = 0; i < 9; i++)
        {
            for (var j = 0; j < 9; j++)
            {
                var field = GetField(i, j);
                field.x = i;
                field.y = j;
            }
        }
    }

    private Field GetField(int x, int y)
    {
        var (big, small) = ConvertCoordinate(x, y);
        return (Field)((Grid)SudokuGrid[big])[small];
    }

    private static (int, int) ConvertCoordinate(int x, int y)
    {
        return (3 * (y / 3) + x / 3, 3 * (y % 3) + x % 3);
    }

    private void ResizeSudokuGrid(object sender, EventArgs e)
    {
        var x = Window.Width;
        var y = Window.Height;
        double l;
        if (x >= y)
        {
            l = y / 4 * 3;
        }
        else
        {
            l = x / 4 * 3;
        }

        SudokuGrid.WidthRequest = l;
        SudokuGrid.HeightRequest = l;
    }

    private void Reload()
    {
        for (int i = 0; i < 9; i++)
        {
            for (int j = 0; j < 9; j++)
            {
                GetField(i, j).digit = sudokuClass[i, j] ?? 0;
            }
        }
    }

    private void Selected(object sender, EventArgs e)
    {
        selectedField = (Field)sender;
    }

    private void NumberButtonClicked(object sender, EventArgs e)
    {
        sudokuClass.selectedNumber = ((Input)sender).digit;
        Reload();
        EndTurn(sender, e);
    }

    private void MenuNumberClicked(object sender, EventArgs e)
    {
        sudokuClass.selectedNumber = byte.Parse(((MenuFlyoutItem)sender).Text);
        Reload();
        EndTurn(sender, e);
    }

    private void Delete(object sender, EventArgs e)
    {
        if (selectedField != null) selectedField.Text = string.Empty;
    }

    private async void Solve(object? sender, EventArgs e)
    {
        var solution = sudokuClass.Solve();
        if (solution) return;
        await DisplayAlert("Unsolvable",
            "There are no solutions to this sudoku", "OK");
    }

    private void ClearAll(object? sender, EventArgs eventArgs)
    {
        selectedField = null;
        foreach (Grid subgrid in SudokuGrid)
            foreach (Button field in subgrid)
                field.Text = "";
        sudokuClass = new VSSudoku();
    }

    private async void NamePopup(object sender, EventArgs e)
    {
        string anwserPlayer1 = await DisplayPromptAsync("",
            "What is the name of player 1?", initialValue: "Player 1");
        namePlayer1 = anwserPlayer1;
        if (anwserPlayer1 == null)
        {
            namePlayer1 = "Player 1";
        }

        playerLabel.Text = $"{namePlayer1} to move";

        string botType = await DisplayActionSheet("Against which kind of bot?",
            null, null, "random bot", "slightly better bot");
        if (botType == "slightly better bot")
        {
            bot = new SlightlyBetterBot();
            Debug.WriteLine("change bot");
        }
    }

    private void ChangePlayer()
    {
        switch (playerToMove)
        {
            case 1:
                playerLabel.Text = $"{namePlayer2} to move";
                playerToMove = 2;
                break;
            case 2:
                playerLabel.Text = $"{namePlayer1} to move";
                playerToMove = 1;
                break;
        }
    }

    private async void CheckWinner()
    {
        bool unique = sudokuClass.CheckWinner();
        if (unique)
        {
            uniqueLabel.Text = "It is Uniquely solvable!";
            if (playerToMove == 1)
            {
                await DisplayAlert($"Game over", $"{namePlayer1} has won!", "Ok");
            }
            else
            {
                await DisplayAlert($"Game over", $"{namePlayer2} has won!", "Ok");
            }
        }
    }

    private void EndTurn(object? sender, EventArgs eventArgs)
    {
        CheckWinner();
        ChangePlayer();
        
        BotTurn(sender, eventArgs);
    }

    private void BotTurn(object? sender, EventArgs eventArgs)
    {
        sudokuClass.sudoku = bot.GetMove(sudokuClass.sudoku);
        Reload();
        CheckWinner();
        ChangePlayer();
    }

    // Methods for highlighting
    private void ClearHighlights()
    {
        foreach (Grid subgrid in SudokuGrid)
        {
            foreach (Field field in subgrid)
            {
                field.BackgroundColor = standard;
            }
        }
    }

    private void HighlightBlock(Field field)
    {
        foreach (Field neighbor in ((Grid)field.Parent).Children)
        {
            neighbor.BackgroundColor = highlight;
        }
    }

    private void HighlightRowColumn(int x, int y)
    {
        for (int i = 0; i < 9; i++)
        {
            GetField(x, i).BackgroundColor = highlight;
        }

        for (int i = 0; i < 9; i++)
        {
            GetField(i, y).BackgroundColor = highlight;
        }
    }
}
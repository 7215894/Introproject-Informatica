using System.Collections.Generic;

namespace SolverLogic;

public class McvSolver
{
    private byte[] solution;
    private byte[] hint;
    /*
     * Public interface for the solver
     * Initiates the InitSolution method which stores the solution to the given sudoku in the class attribute
     * this method returns said attribute which might be null if you fed it an unsolvable sudoku.
     */
    public byte[]? GetSolution(byte[] sudoku)
    {
        solution = null;
        InitSolution(sudoku, new Stack<byte>(300));
        return solution;
    }

    /*
     * This method gets the most constrained index meaning the sudoku square with least possible numbers and keeps
     * trying to guess the solution dynamically generating the most constrained at each level of the search tree.
     * It then tries all possible moves for this index with a recursive call using a stack to keep track of moves
     * and terminates itself once it finds that one of the recursive calls found a valid solution.
     * This method will return the first solution found in case of multiple possible solutions and ignore the others.
     */
    private void InitSolution(byte[] sudoku, Stack<byte> moves)
    {
        int index = SudokuUtils.GetMostConstrained(sudoku);
        if (index == 81)
        {
            solution = sudoku;
            return;
        }

        int before = moves.Count;
        SudokuUtils.GetPossibleMoves(sudoku, index, moves);
        int after = moves.Count;
        for(var i = 0; i < (after - before); i++)
        {
            sudoku[index] = moves.Pop();
            InitSolution(sudoku, moves);
            if (solution != null) 
                return;
            
        }
        sudoku[index] = 0;
    }
    
}
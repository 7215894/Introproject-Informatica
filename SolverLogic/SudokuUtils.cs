using System.Collections.Generic;

namespace SolverLogic;

public class SudokuUtils
{
    
    /*
     * This method checks for a given square on a sudoku what moves are valid and pushes said moves on the provided
     * stack to store them.
     */
    public static void GetPossibleMoves(byte[] sudoku, int index, Stack<byte> moves)
    {
        for (byte j = 1; j < 10; j++)
        {
            if (IsValidMove(j, index, sudoku)) moves.Push(j);
        }
    }
    
    /*
     * This method combines the rules of sudoku of not having two the same numbers in one column row or 3x3 square
     * into one method by checking if the provided number is already in the same row, column, or square in the provided
     * sudoku. The method returns true if placing the number at the specified index doesn't violate any of these rules.
     */
    public static bool IsValidMove(byte value, int index, byte[] sudoku)
    {
        return ValidateRow(value, index, sudoku) && ValidateColumn(value, index, sudoku) && ValidateGroup(value, index, sudoku);
    }
    
    /*
     * This method checks whether the provided value is present in the column corresponding to the given index returning
     * false if the value is already present as then the move would be invalid according the rules of sudoku.
    */
    private static bool ValidateColumn(byte value, int index, byte[] sudoku)
    {
        for (int i = 1; i < 9; i++)
        {
            if (sudoku[((i * 9) + index) % 81] == value) return false;
        }
        return true;
    }

    /*
     * This method checks whether the provided value is present in the row corresponding to the given index returning
     * false if the value is already present as then the move would be invalid according the rules of sudoku.
     */
    private static bool ValidateRow(byte value, int index, byte[] sudoku)
    {
        int StartingPoint = index - (index % 9);
        for (int i = StartingPoint; i <= StartingPoint + 8; i++)
        {
            if (sudoku[i] == value) return false;
        }
        return true;
    }

    
    /*
     * This method checks whether the provided value is present in the 3x3 square corresponding to the given index returning
     * false if the value is already present as then the move would be invalid according the rules of sudoku.
    */
    private static bool ValidateGroup(byte value, int index, byte[] sudoku)
    {
        int rowNumber = LineClasser((index - (index % 9) )/ 9);
        int columNumber = LineClasser(index % 9);
        for (int i = 0; i < 3; i++)
        {
            int coordinate = ((i + rowNumber) * 9) + columNumber;
            if (sudoku[coordinate] == value || sudoku[coordinate + 1] == value ||
                sudoku[coordinate + 2] == value) return false;
        }
        
        return true;
    }
    
    /*
     * This method is used to find the upper-left corner of a 3x3 square making it easier to iterate over the correct
     * indexes of the byte array as corresponds to an actual sudoku grid.
     */
    private static int LineClasser(int lineNumber)
    {
        if (lineNumber < 3) return 0;
        if (lineNumber is >= 3 and < 6) return 3;
        return 6;
    }
    
    /*
     * Checks the amount of moves for each square of the sudoku keeping the index with the minimal amount of moves.
     * Therefore finding the most constrained variable and returning this. This method is used as this prioritization
     * leads to performance gains.
     */
    public static int GetMostConstrained(byte[] sudoku)
    {
        int index = 81;
        int count = 10;
        for(int i = 0; i < 81; i++)
        {   
            if(sudoku[i] != 0) continue;
            int iterativeCount = GetAmountOfMoves(sudoku, i);
            if(iterativeCount < count)
            {
                index = i;
                count = iterativeCount;
            }
        }
        return index;
    }
    /*
     * Checks the amount of moves for each square of the sudoku keeping the index with the maximal amount of moves.
     * Therefore finding the most least variable and returning this. This method is used as it helps bots and can give
     * bigger hints.
     */
    public static int GetLeastConstrained(byte[] sudoku)
    {
        int index = 81;
        int count = 0;
        for(int i = 0; i < 81; i++)
        {   
            if(sudoku[i] != 0) continue;
            int iterativeCount = GetAmountOfMoves(sudoku, i);
            if(iterativeCount > count)
            {
                index = i;
                count = iterativeCount;
            }
        }
        return index;
    }

    /*
    * This methods uses the IsValidMove method to check how many moves exist for a certain square on the sudoku grid. 
    */
    private static int GetAmountOfMoves(byte[] sudoku, int index)
    {
        int count = 0;
        for (byte i = 1; i < 10; i++)
        {
            if (IsValidMove(i, index, sudoku)) count++;
        }
        return count;
    }
    
}
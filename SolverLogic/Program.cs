using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text.RegularExpressions;
using SolverLogic.bin;

namespace SolverLogic;

public class Program
{
    private static List<byte[]> solvableSudokus = new List<byte[]>(1000);
    private static List<byte[]> solvedSudokus = new List<byte[]>(1000);

    public static void GeneratorDiagnostics()
    {
        List<byte[]> suds = new List<byte[]>(1000);
        SudokuGenerator gen = new SudokuGenerator();
        DateTime before = DateTime.Now;
        for (int i = 0; i < 100; i++)
        {
            suds.Add(gen.GenerateSudoku(27));
            Console.WriteLine("generated " + i);
        }
        DateTime after = DateTime.Now;
        TimeSpan duration = after.Subtract(before);
        Console.WriteLine("Total time spent generating: " + duration.ToString());
        //Console.WriteLine("Average time spent generating: " + duration.Multiply((float)1/suds.Count).ToString());
        for (int u = 0; u < 10; u++)
        {
            for (int i = 0; i < 9; i++)
            {
                string s = "";
                for (int j = 0; j < 9; j++)
                {
                    s += suds[u][i * 9 + j];
                }

                Console.WriteLine(s);
            }
            Console.WriteLine();
            Console.WriteLine();
        }
    }

    public static void SolverDiagnostics()
    {
        LoadHardSudokus();
        List<byte[]> answers = new List<byte[]>(10000);
        DateTime before = DateTime.Now;
        int count = 0;
        McvSolver solver = new McvSolver();
        foreach (var sudoku in solvableSudokus)
        {
            answers.Add(solver.GetSolution(sudoku));
            count++;
            Console.WriteLine("Solved " + count);
        }
        DateTime after = DateTime.Now;
        TimeSpan duration = after.Subtract(before);
        Console.WriteLine("Total time spent solving: " + duration.ToString());
        Console.WriteLine("Average time spent solving: " + duration.Multiply((float)1/answers.Count).ToString());
        bool allCorrect = true;
        for (int i = 0; i < answers.Count; i++)
        {
            if (!answers[i].SequenceEqual(solvedSudokus[i]))
            {
                allCorrect = false;
            }
        }
        Console.WriteLine("The statement all solutions were correct is: " + allCorrect);
    }

    public static void VsDiagnosticsUniquelySolvable()
    {
        var sudokus = LoadVsSudokus();
        VsSolver solver = new VsSolver();
        bool allCorrect = true;
        int i = 1;
        DateTime before = DateTime.Now;
        foreach (var sudoku in sudokus)
        {
            if (!solver.IsUniquelySolvable(sudoku)) allCorrect = false;
            Console.WriteLine("Solved " + i);
            i++;
        }
        DateTime after = DateTime.Now;
        TimeSpan duration = after.Subtract(before);
        Console.WriteLine("Total time spent solving: " + duration.ToString());
        Console.WriteLine("Average time spent solving: " + duration.Multiply((float)1/sudokus.Count).ToString());
        Console.WriteLine("All tests were successful: " + allCorrect);
    }
    public static void VsDiagnosticsNotUniquelySolvable()
    {
        var sudokus = LoadEmptySudokus();
        VsSolver solver = new VsSolver();
        bool allCorrect = true;
        int i = 1;
        DateTime before = DateTime.Now;
        foreach (var sudoku in sudokus)
        {
            if (solver.IsUniquelySolvable(sudoku)) allCorrect = false;
            Console.WriteLine("Solved " + i);
            i++;
        }
        DateTime after = DateTime.Now;
        TimeSpan duration = after.Subtract(before);
        Console.WriteLine("Total time spent solving: " + duration.ToString());
        Console.WriteLine("Average time spent solving: " + duration.Multiply((float)1/sudokus.Count).ToString());
        Console.WriteLine("All tests were successful: " + allCorrect);
    }

    //Used https://stackoverflow.com/questions/239103/convert-char-to-int-in-c-sharp for converting char to int
    public static byte[] SudokuParser(String s)
    {
        char[] cijfers = s.ToArray();
        byte[] numbers = new byte[cijfers.Length];
        for (int i = 0; i < cijfers.Length; i++)
        {
            
            numbers[i] = (byte) (cijfers[i] - '0');
        }
        return numbers;
    }

    public static void LoadHardSudokus()
    {
        StreamReader fileReader = new StreamReader("Sudokus.txt");
        fileReader.ReadLine();
        String line = fileReader.ReadLine();
        int amount = 1000;
        while (line != null && amount > 0)
        {
            string[] solvAndSol = Regex.Split(line, ",");
            solvableSudokus.Add(SudokuParser(solvAndSol[0]));
            solvedSudokus.Add(SudokuParser(solvAndSol[1]));
            line = fileReader.ReadLine();
            amount--;
        }
        Console.WriteLine("End of parsing");
    }

    public static List<byte[]> LoadVsSudokus()
    {
        int amount = 100;
        List<byte[]> sudokus = new List<byte[]>(amount);
        StreamReader fileReader = new StreamReader("all_17_clue_sudokus.txt");
        fileReader.ReadLine();
        String line = fileReader.ReadLine();
        while (line != null && amount > 0)
        {
            sudokus.Add(SudokuParser(line));
            line = fileReader.ReadLine();
            amount--;
        }
        
        return sudokus;
    }

    public static List<byte[]> LoadEmptySudokus()
    {
        int amount = 100;
        List<byte[]> sudokus = new List<byte[]>(amount);
        for (int i = amount; i > 0; i--)
        {
            sudokus.Add(new byte[81]);
        }

        return sudokus;
    }
}
using System.Collections.Generic;

namespace SolverLogic;

public class VsSolver
{
    private bool oneSolutionFound;
    private bool twoSolutionsFound;
    
    /*
     * This method initializes the class attributes and starts the SolutionsFinder method. It functions as the public
     * interface of this solver class.
     */
    public bool IsUniquelySolvable(byte[] sudoku)
    {
        oneSolutionFound = false;
        twoSolutionsFound = false;
        SolutionsFinder(sudoku, new Stack<byte>(300));
        return !twoSolutionsFound && oneSolutionFound;
    }

    public bool IsNotSolvable(byte[] sudoku)
    {
        oneSolutionFound = false;
        twoSolutionsFound = false;
        SolutionsFinder(sudoku, new Stack<byte>(300));
        return !twoSolutionsFound && !oneSolutionFound;
    }

    /*
     * This is a recursive solver very similar to the one in the McvSolver class the only real difference is instead
     * of actually setting the solution it sets flags for how many solutions it has found. If it encounters a second
     * valid solution the method terminates as it has found the sudoku to be not uniquely solvable.
     */
    private void SolutionsFinder(byte[] sudoku, Stack<byte> moves)
    {
        int index = SudokuUtils.GetMostConstrained(sudoku);
        if (index == 81)
        {
            if (oneSolutionFound)
            {
                twoSolutionsFound = true;
                return;
            }

            oneSolutionFound = true;
            return;
        }
        int before = moves.Count;
        SudokuUtils.GetPossibleMoves(sudoku, index, moves);
        int after = moves.Count;
        for(var i = 0; i < (after - before); i++)
        {
            sudoku[index] = moves.Pop();
            SolutionsFinder(sudoku, moves);
            if (twoSolutionsFound) 
                return;
            
        }
        sudoku[index] = 0;
    }
    
}
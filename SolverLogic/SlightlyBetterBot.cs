namespace SolverLogic;
using System;


public class SlightlyBetterBot : VsBot
{
    private McvSolver solver = new McvSolver();
        
    /*
     * This bot tries to be better than RandomBot by reducing the number of possibilities as quickly as possible. This
     * strategy of course has a few weaknesses but should the human opponent make any mistake on the 17th clue this bot
     * will punish them.
     */
    public override byte[] GetMove(byte[] sudoku)
    {
        int index = SudokuUtils.GetLeastConstrained(sudoku);
        byte[] backup = new byte[81];
        Array.Copy(sudoku, backup, 81);
        byte[] solution = solver.GetSolution(backup);
        sudoku[index] = solution[index];
        return sudoku;
    }
}
﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SolverLogic
{
    public abstract class VsBot
    {
        public abstract byte[] GetMove(byte[] sudoku);
    }
}

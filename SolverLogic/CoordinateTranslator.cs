﻿using System;
using System.Collections.Generic;
using System.IO;

namespace SolverLogic;

public class CoordinateTranslator
{
    public static byte[] CordsListToByteArray(List<SudokuCoordinate> cords)
    {
        if (cords.Count > 81) throw new InvalidDataException("You shall not abuse my methods mortal");
        byte[] returner = new byte[81];
        foreach (var cord in cords)
        {
            returner[(cord.getRow() - 1) * 9 + (cord.getColumn() - 1)] = Convert.ToByte(cord.getValue());
        }

        return returner;
    }
}
using System;

namespace SolverLogic;

public class Hinter
{
    private McvSolver solver = new McvSolver();
    
    /*
     * Returns the old sudoku with an extra random index correctly filled in
     */
    public byte[] GetRandomHint(byte[] sudoku)
    {
        byte[] backup = new byte[81];
        Array.Copy(sudoku, backup, 81);
        byte[] solution = solver.GetSolution(sudoku);
        Random rand = new Random();
        bool foundGoodIndex = false;
        while (!foundGoodIndex)
        {
            int index = rand.Next(0, 81);
            if (backup[index] == 0)
            {
                backup[index] = solution[index];
                foundGoodIndex = true;
            }
        }
        return backup;
    }
    /*
     * Returns the old sudoku with an index that is the most constrained correctly filled in. As this index only had a
     * few possibilities the information gained by the user receiving the hint will be minimal.
     */
    public byte[] GetSmallHint(byte[] sudoku)
    {
        byte[] backup = new byte[81];
        Array.Copy(sudoku, backup, 81);
        byte[] solution = solver.GetSolution(sudoku);
        int index = SudokuUtils.GetMostConstrained(backup);
        backup[index] = solution[index];
        return backup;
    }
    /*
     * Returns the old sudoku with an index that is the least constrained correctly filled in. As this index had many
     * possibilities the gain for the user receiving the hint will be maximal.
     */
    public byte[] GetBigHint(byte[] sudoku)
    {
        byte[] backup = new byte[81];
        Array.Copy(sudoku, backup, 81);
        byte[] solution = solver.GetSolution(sudoku);
        int index = SudokuUtils.GetLeastConstrained(backup);
        backup[index] = solution[index];
        return backup;
    }
}
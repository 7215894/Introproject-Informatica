using System;

namespace SolverLogic;

public class RandomBot : VsBot
{
    Random rand = new Random();
    private McvSolver solver = new McvSolver();
    public override byte[] GetMove(byte[] sudoku)
    {
        int index = rand.Next(0, 81);
        byte value = (byte) rand.Next(1, 10);
        if (sudoku[index] != 0) return GetMove(sudoku);
        byte[] backup = new byte[81];
        Array.Copy(sudoku, backup, 81);
        if (SudokuUtils.IsValidMove(value, index, sudoku) && solver.GetSolution(backup) != null)
        {
            sudoku[index] = value;
            return sudoku;
        }
        return GetMove(sudoku);
    }

}
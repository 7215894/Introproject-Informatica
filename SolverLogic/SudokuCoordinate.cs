﻿using System;

namespace SolverLogic;

public class SudokuCoordinate
{
    private int row;
    private int column;
    private int squareValue;

    public SudokuCoordinate(int xPos, int yPos, int value)
    {
        row = xPos;
        column = yPos;
        squareValue = value;
    }

    public bool EqualPosition(SudokuCoordinate other)
    {
        return this.row == other.getRow() && this.column == other.getColumn();
    }

    public int getRow()
    {
        return row;
    }

    public int getColumn()
    {
        return column;
    }

    public int getValue()
    {
        return squareValue;
    }

    public String toString()
    {
        return "Row: " + row + " Column: " + column + " Value: " + squareValue + "\n";
    }

}
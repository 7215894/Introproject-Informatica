using System;

namespace SolverLogic.bin;

public class SudokuGenerator
{
    Random rand = new Random();
    private byte[] sudoku = new byte[81];
    private VsSolver vsSolver = new VsSolver();
    private McvSolver solver = new McvSolver();

    public byte[] GenerateSudoku(int clues)
    {
        sudoku = new byte[81];
        GetStartPositions();
        sudoku = solver.GetSolution(sudoku);
        Obfuscator(clues);
        byte[] tester = new byte[81];
        Array.Copy(sudoku, tester, 81);
        if(vsSolver.IsUniquelySolvable(tester))return sudoku;
        return null;
    }

    private void GetStartPositions()
    {
        for(int i = 0; i < 8; i++)
        {
            int index = rand.Next(0, 81);
            byte value = (byte) rand.Next(1, 10);
            if(SudokuUtils.IsValidMove(value, index, sudoku)) sudoku[index] = value;
            else i--;
        }
    }

    private void Obfuscator(int clues)
    {
        int counter = 0;
        while (counter < 81 - clues)
        {
            int index = rand.Next(0, 81);
            if (sudoku[index] != 0 && IsValidSudoku(index))
            {
                sudoku[index] = 0;
                counter++;
            }
        }

    }

    private bool IsValidSudoku(int index)
    {
        byte[] backup = new byte[81];
        Array.Copy(sudoku, backup, 81);
        backup[index] = 0;
        if (vsSolver.IsUniquelySolvable(backup)) return true;
        return false;
        
    }
    
}
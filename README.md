Welcome to our Sudoku Solver

# Description

This project is a sudoku solver and an implementation of a [two player sudoku](https://www.codecup.nl/sudoku/rules.php) (link in Dutch). It's written in C# with .NET MAUI, because of it's portability.
This application can be used to solve a sudoku or to play a sudoku against another player or a bot. 

# Building

The project is written in C# and XAML, using .NET 8 and .NET MAUI (Multiplatform App UI). The current supported platforms are Mac, iOS and Windows. The windows version does not support Rider. To build the project, you should install .NET and .NET MAUI. You will need at least .NET version 8. To build on your platform without installing more components than needed you might need to remove platforms you will not use from the dependencies in UI.csproj. 

# Using
When you open our application, you will land on the main menu, where you can choose if you want to solve a sudoku or play a vs sudoku.
You can place numbers by clicking on the field and enter numbers by using the number buttons to the right, or typing the number using your keyboard.
To the left are buttons with other actions you can take.
The solve button wil give you a solution to the current sudoku in the sudoku grid.
The delete button wil delete the number of the selected field.
The undo button will undo the last action and the redo button wil redo that action.
The clear button will clear the entire field.
The hint button wil give you options on what kind of hint you want.
The Generate sudoku will give you options on what difficluty it should generate.
In the backend there is a program class that can be used for diagnostics.

##  Goal:
The goal of two player sudoku is to make the sudoku uniquely solvable so that there is only one solution on your turn.

##  Turn:
A player can, on their turn make a move by placing a number in an empty cell. Players take turns alternatly.
When you input an invalid move, you skip your turn.
##  How to win:
If there is only one solution left, you win!


# Credits
By Max Maes, Guido Pagie, Kerlijn Keetman, Emir Keskelinç and Aurora Uitslager
